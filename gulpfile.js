var gulp           = require('gulp'),
    gutil          = require('gulp-util'),
    sass           = require('gulp-sass'),
    concat         = require('gulp-concat'),
    uglify         = require('gulp-uglify'),
    rename         = require('gulp-rename'),
    notify         = require('gulp-notify'),
    cleanCSS       = require('gulp-clean-css'),
    autoprefixer   = require('gulp-autoprefixer'),
    browserSync    = require('browser-sync');

var rootPath = 'app';

var src = {
    js: rootPath + '/js',
    css: rootPath + '/css',
    lib: rootPath + '/libs',
    sass: rootPath + '/sass/**/*.{sass,scss}'
}

gulp.task('js', function() {
    return gulp.src([
        src.lib + '/jquery-3.2.1.min.js',
        src.lib + '/jquery.fancybox.min.js',
        src.lib + '/isotope.js',
        src.lib + '/masonry.js',
        src.lib + '/owl.carousel.min.js',
        src.lib + '/remodal.min.js',
    ])
    .pipe(concat('lib.js'))
    .pipe(uglify()) // Минимизировать весь js (на выбор)
    .pipe(gulp.dest(src.js))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: rootPath
        },
        notify: false
    });
});

gulp.task('sass', function() {
    return gulp.src(src.sass)
    .pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
    .pipe(autoprefixer(['last 15 versions']))
    .pipe(cleanCSS()) // Опционально, закомментировать при отладке
    .pipe(gulp.dest(src.css))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['sass', 'js', 'browser-sync'], function() {
    gulp.watch(src.sass, ['sass']);
    gulp.watch([src.lib + '/**/*.js', src.js + '/app.js'], ['js']);
    gulp.watch(rootPath + '/*.html', browserSync.reload);
});

gulp.task('default', ['watch']);
